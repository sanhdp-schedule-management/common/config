package config

import (
	"fmt"
	"os"

	Viper "github.com/spf13/viper"
	"gitlab.com/sanhdp-schedule-management/common/adapter"
)

// Config struct ..
type Config struct {
	Name     string		`mapstructure:"name"`
	Port     int 		`mapstructure:"port"`
	Version  string     `mapstructure:"version"`
	Debug    bool       `mapstructure:"debug"`
	Mysql    adapter.Mysqls
}

var config *Config

func init() {
	var folder string

	env := os.Getenv("APPLICATION_ENV")

	switch env {
	case "master", "dev":
		folder = env
	default:
		folder = "dev"
	}

	path := fmt.Sprintf("config/%v", folder)

	//Get base config
	config = new(Config)
	fetchDataToConfig(path, "base", config)

	//Get all sub config
	fetchDataToConfig(path, "mysql", &(config.Mysql))
}

func fetchDataToConfig(configPath, configName string, result interface{}) {
	viper := Viper.New()
	viper.AddConfigPath(configPath)
	viper.SetConfigName(configName)

	err := viper.ReadInConfig() // Find and read the config file
	if err == nil {             // Handle errors reading the config file
		err = viper.Unmarshal(result)
		if err != nil { // Handle errors reading the config file
			panic(fmt.Errorf("Fatal error config file: %s", err))
		}
	}
}

// GetConfig func
func GetConfig() *Config {
	return config
}
